from django.shortcuts import render, redirect
from .forms import DocumentForm
from .models import Documents
import time
from django.contrib import messages
from .models import Documents
from printf.settings import MEDIA_ROOT
# Create your views here.


import sys
import os
import subprocess
import re

def document_upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():

            file_name = request.FILES['document'].name
            
            order = form.save(commit=False)
            order.save()
            order.code = str(order.id)+ '-' + str(time.time())[:-8]
            order.file_name = file_name
            order.amount = 250

            order.save()
            converted = True

            if file_name.endswith('.doc') | file_name.endswith('.docx'):
                converted = doc2pdf(order.id)
            
            if converted:
                messages.info(request, 'Код документа: %s'%(order.code))
            else:
                messages.info(request, 'Не удалос конвертировать')

            return redirect('student:index')
    else:
        form = DocumentForm()
    return render(request, 'index.html', {
        'form': form
    })


def doc2pdf(order_id, timeout=None):

    order = Documents.objects.get(pk = order_id)

    source = os.path.join(MEDIA_ROOT, str(order.document))

    folder = os.path.join(MEDIA_ROOT, 'test')

    args = ['libreoffice', '--headless', '--convert-to', 'pdf', '--outdir', folder, source]

    try:
        process = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=timeout)
        filename = re.search('-> (.*?) using filter', process.stdout.decode())
        
    except Exception as ex:
        return False

    if filename is None:
        return False
    else:
        os.remove(source)
        converted_file = filename.group(1).split('/test/')[-1]
        order.document = 'test/' + converted_file
        order.save()
        return True


    

