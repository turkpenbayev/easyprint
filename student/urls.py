from django.urls import path
from django.contrib.auth import views as auth_views
from . import views


app_name = 'student'

urlpatterns = [
    path('', views.document_upload, name = 'index')
]